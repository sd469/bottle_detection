import fiftyone as fo
import fiftyone.zoo as foz
import torch
from fiftyone import ViewField as F

import transforms as T
from Dataset import FiftyOneTorchDataset
from Pipeline import get_model, do_training, add_detections
device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
object_list = ["bottle"]
fo_dataset = foz.load_zoo_dataset(
    "coco-2017",
    split="train",
    label_types=["detections"],
    max_samples=3000,
    classes=object_list,
)
session = fo.launch_app(fo_dataset, desktop=True)
# dataset_type = fo.types.COCODetectionDataset
# dataset = fo.Dataset.from_dir(
#     dataset_dir=dataset_dir,
#     dataset_type=dataset_type,
#     name=name,
# )
dataset = fo_dataset.filter_labels("ground_truth",
                                   F("label").is_in(object_list))
train_view = dataset.take(2000, seed=51)
test_view = dataset.exclude([s.id for s in train_view])
train_transforms = T.Compose([T.PILToTensor(), T.RandomHorizontalFlip(0.5)])
test_transforms = T.Compose([T.PILToTensor()])
torch_dataset = FiftyOneTorchDataset(train_view, train_transforms,
                                     classes=object_list)
torch_dataset_test = FiftyOneTorchDataset(test_view, test_transforms,
                                          classes=object_list)
model = get_model(len(object_list) + 1)
# do_training(model, torch_dataset, torch_dataset_test, train_batch_size=5, test_batch_size=5, num_workers=0,
#             num_epochs=100, checkpoint_save_freq=20)
checkpoint=torch.load("Checkpoints/Checkpoint80.pth.tar", device)
model.load_state_dict(checkpoint['model_state_dicts'])
add_detections(model, torch_dataset_test, fo_dataset, field_name="predictions")
results = fo.evaluate_detections(
    test_view,
    "predictions",
    classes=object_list,
    eval_key="eval",
    compute_mAP=True
)
results.mAP()
results.print_report()

session.view = test_view

session.wait()
